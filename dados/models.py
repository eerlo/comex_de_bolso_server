#-*- coding: utf-8 -*-
from django.db import models

# Create your models here.
class FonteNoticia(models.Model):
    nome = models.CharField(u'Nome', max_length=100)
    imagem = models.ImageField(u'Imagem', upload_to=u'imagens_fontes_noticias')
    url_feed = models.URLField(u'URL do feed', unique=True)
    ativo = models.BooleanField(u'Ativo', default=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = u'Fonte de Notícia'
        verbose_name_plural = u'Fontes de Notícias'

    def __unicode__(self):
        return self.nome

class Noticia(models.Model):
    fonte = models.ForeignKey(FonteNoticia, verbose_name=u'Fonte da Notícia')
    titulo = models.CharField(u'Título', max_length=300)
    link = models.CharField(
        u'Link Original da Notícia', max_length=400, unique=True)
    descricao = models.TextField(u'Descrição')
    data_publicacao = models.DateTimeField(u'Data da publicação')
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = u'Notícia'
        verbose_name_plural = u'Notícias'

    def __unicode__(self):
        return self.titulo


#class Moeda(models.Model):
#    codigo = models.CharField(u'Código', max_length=3)
#    tipo = models.CharField(u'Tipo', max_length=1)
#    moeda = models.CharField(u'Moeda', max_length=3)
#    taxa_compra = models.DecimalField(u'Taxa Compra', decimal_places=10)
#    taxa_venda = models.DecimalField(u'Taxa Venda', decimal_places=10)
#    paridade_compra = models.DecimalField(u'Paridade Compra', decimal_places=10)
#    paridade_venda = models.DecimalField(u'Paridade Venda', decimal_places=10)
#    created_on = models.DateTimeField(auto_now_add=True)
#    updated_on = models.DateTimeField(auto_now=True)


class Pais(models.Model):
    nome = models.CharField(u'Nome', max_length=50)

    class Meta:
        verbose_name = u'País'
        verbose_name_plural = u'Países'

    def __unicode__(self):
        return self.nome

class UF(models.Model):
    nome = models.CharField(u'Nome', max_length=50)
    pais = models.ForeignKey(Pais, verbose_name=u'País')

    class Meta:
        verbose_name = u'UF'
        verbose_name_plural = u'UFs'

    def __unicode__(self):
        return self.nome

class Cidade(models.Model):
    nome = models.CharField(u'Nome', max_length=50)
    uf = models.ForeignKey(UF, verbose_name=u'UF')

    class Meta:
        verbose_name = u'Cidade'
        verbose_name_plural = u'Cidades'

    def __unicode__(self):
        return self.nome

class Empresa(models.Model):
    nome = models.CharField(u'Nome', max_length=50)
    logo = models.ImageField(u'Imagem', upload_to=u'imagens_empresas')
    cidade = models.ForeignKey(Cidade, verbose_name=u'Cidade')
    endereco = models.CharField(u'Endereço', max_length=150)
    telefones = models.CharField(u'Telefones', max_length=150)
    email = models.CharField(u'E-mail', max_length=50, null=True, blank=True)
    site = models.CharField(u'Site', max_length=50, null=True, blank=True)
    descricao = models.TextField(u'Descrição', max_length=1000,
        null=True, blank=True)

    def __unicode__(self):
        return self.nome

    class Meta:
        verbose_name = u'Empresa'
        verbose_name_plural = u'Empresas'

class VagaEmpregoEmpresa(models.Model):
    empresa = models.ForeignKey(
        Empresa,
        verbose_name=u'Empresa',
        related_name=u'vagas_emprego')
    titulo = models.CharField(u'Título', max_length=50)
    descricao = models.TextField(u'Descrição', max_length=1000)
    ativo = models.BooleanField(u'Ativo', default=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = u'Vaga de Emprego'
        verbose_name_plural = u'Vagas de Emprego'

    def __unicode__(self):
        return self.titulo