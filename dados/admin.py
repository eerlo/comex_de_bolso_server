#-*- coding: utf-8 -*-
from django.contrib import admin
from dados.models import (Empresa, Cidade, UF, Pais, FonteNoticia,
    Noticia, VagaEmpregoEmpresa)

# Register your models here.
admin.site.register(Empresa)
admin.site.register(VagaEmpregoEmpresa)
admin.site.register(Cidade)
admin.site.register(UF)
admin.site.register(Pais)
admin.site.register(FonteNoticia)
admin.site.register(Noticia)
