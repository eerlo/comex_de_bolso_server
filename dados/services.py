#-*- coding: utf-8 -*-
import requests
from lxml import html
import cssselect
import json
from django.core.cache import cache
from dados.models import Empresa, Noticia

class MoedasServiceProvider(object):
    url = 'https://www3.bcb.gov.br/ptax_internet/consultarTodasAsMoedas'
    url += '.do?method=consultaTodasMoedas'

    def __init__(self):
        self._popula_moedas()

    def _popula_moedas(self):
        self.moedas = []
        html_parseado = html.fromstring(requests.get(self.url).content)
        for tr in html_parseado.cssselect(u'table tr'):
            tds = tr.cssselect(u'td')
            if tds:
                self.moedas.append(
                    {u'codigo': tds[0].text_content().strip(),
                     u'tipo': tds[1].text_content().strip(),
                     u'moeda': tds[2].text_content().strip(),
                     u'taxa_compra': tds[3].text_content().strip(),
                     u'taxa_venda': tds[4].text_content().strip()}
                )

    def retorna_dados(self):
        return json.dumps(self.moedas)


class EmpresasServiceProvider(object):

    def __init__(self):
        self._popula_empresas()

    def _popula_empresas(self):
        self.empresas = []
        for empresa in Empresa.objects.all():
            atual = {u'nome': empresa.nome,
                     u'logo': empresa.logo.url,
                     u'cidade': empresa.cidade.nome,
                     u'uf': empresa.cidade.uf.nome,
                     u'pais': empresa.cidade.uf.pais.nome,
                     u'endereco': empresa.endereco,
                     u'telefones': empresa.telefones}
            if empresa.email:
                atual[u'email'] = empresa.email
            if empresa.site:
                atual[u'site'] = empresa.site
            if empresa.descricao:
                atual[u'descricao'] = empresa.descricao

            atual[u'vagas_emprego'] = []
            for vaga_emprego in empresa.vagas_emprego.filter(ativo=True):
                atual[u'vagas_emprego'].append(
                    {u'titulo': vaga_emprego.titulo,
                     u'descricao': vaga_emprego.descricao}
                )

            self.empresas.append(atual)

    def retorna_dados(self):
        return json.dumps(self.empresas)

class NoticiasServiceProvider(object):

    def __init__(self):
        self._popula_noticias()

    def _popula_noticias(self):
        self.noticias = []
        for noticia in Noticia.objects.all():
            atual = {u'titulo': noticia.titulo,
                     u'link': noticia.link,
                     u'descricao': noticia.descricao,
                     u'data_publicacao': \
                        noticia.data_publicacao.strftime(u'%A, %d/%m/%Y'),
                    u'id': noticia.pk,
                    u'fonte': noticia.fonte.nome}
            
            self.noticias.append(atual)

    def retorna_dados(self):
        return json.dumps(self.noticias)
