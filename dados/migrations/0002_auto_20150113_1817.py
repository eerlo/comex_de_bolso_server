# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dados', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='VagaEmpregoEmpresa',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=50, verbose_name='T\xedtulo')),
                ('descricao', models.TextField(max_length=1000, verbose_name='Descri\xe7\xe3o')),
                ('ativo', models.BooleanField(default=True, verbose_name='Ativo')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('empresa', models.ForeignKey(related_name='vagas_emprego', verbose_name='Empresa', to='dados.Empresa')),
            ],
            options={
                'verbose_name': 'Vaga de Emprego',
                'verbose_name_plural': 'Vagas de Emprego',
            },
            bases=(models.Model,),
        ),
        migrations.AlterModelOptions(
            name='cidade',
            options={'verbose_name': 'Cidade', 'verbose_name_plural': 'Cidades'},
        ),
        migrations.AlterModelOptions(
            name='fontenoticia',
            options={'verbose_name': 'Fonte de Not\xedcia', 'verbose_name_plural': 'Fontes de Not\xedcias'},
        ),
        migrations.AlterModelOptions(
            name='noticia',
            options={'verbose_name': 'Not\xedcia', 'verbose_name_plural': 'Not\xedcias'},
        ),
        migrations.AlterModelOptions(
            name='pais',
            options={'verbose_name': 'Pa\xeds', 'verbose_name_plural': 'Pa\xedses'},
        ),
        migrations.AlterModelOptions(
            name='uf',
            options={'verbose_name': 'UF', 'verbose_name_plural': 'UFs'},
        ),
        migrations.AlterField(
            model_name='fontenoticia',
            name='url_feed',
            field=models.URLField(unique=True, verbose_name='URL do feed'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='noticia',
            name='link',
            field=models.CharField(unique=True, max_length=400, verbose_name='Link Original da Not\xedcia'),
            preserve_default=True,
        ),
    ]
