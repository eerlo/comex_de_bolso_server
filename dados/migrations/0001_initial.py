# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Cidade',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=50, verbose_name='Nome')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Empresa',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=50, verbose_name='Nome')),
                ('logo', models.ImageField(upload_to='imagens_empresas', verbose_name='Imagem')),
                ('endereco', models.CharField(max_length=150, verbose_name='Endere\xe7o')),
                ('telefones', models.CharField(max_length=150, verbose_name='Telefones')),
                ('email', models.CharField(max_length=50, null=True, verbose_name='E-mail', blank=True)),
                ('site', models.CharField(max_length=50, null=True, verbose_name='Site', blank=True)),
                ('descricao', models.TextField(max_length=1000, null=True, verbose_name='Descri\xe7\xe3o', blank=True)),
                ('cidade', models.ForeignKey(verbose_name='Cidade', to='dados.Cidade')),
            ],
            options={
                'verbose_name': 'Empresa',
                'verbose_name_plural': 'Empresas',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FonteNoticia',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=100, verbose_name='Nome')),
                ('imagem', models.ImageField(upload_to='imagens_fontes_noticias', verbose_name='Imagem')),
                ('url_feed', models.URLField(verbose_name='URL do feed')),
                ('ativo', models.BooleanField(default=True, verbose_name='Ativo')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Noticia',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=300, verbose_name='T\xedtulo')),
                ('link', models.CharField(max_length=400, verbose_name='Link Original da Not\xedcia')),
                ('descricao', models.TextField(verbose_name='Descri\xe7\xe3o')),
                ('data_publicacao', models.DateTimeField(verbose_name='Data da publica\xe7\xe3o')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('fonte', models.ForeignKey(verbose_name='Fonte da Not\xedcia', to='dados.FonteNoticia')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Pais',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=50, verbose_name='Nome')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UF',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=50, verbose_name='Nome')),
                ('pais', models.ForeignKey(verbose_name='Pa\xeds', to='dados.Pais')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='cidade',
            name='uf',
            field=models.ForeignKey(verbose_name='UF', to='dados.UF'),
            preserve_default=True,
        ),
    ]
