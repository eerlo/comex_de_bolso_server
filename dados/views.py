#-*- coding: utf-8 -*-
from django.shortcuts import render
from django.views.generic import View
from dados.services import (MoedasServiceProvider, EmpresasServiceProvider,
    NoticiasServiceProvider)
from django.http import HttpResponse

class RetornaMoedasView(View):

    def get(self, request):
        return HttpResponse(MoedasServiceProvider().retorna_dados())


class RetornaEmpresasView(View):

    def get(self, request):
        return HttpResponse(EmpresasServiceProvider().retorna_dados())

class RetornaNoticiasView(View):

    def get(self, request):
        return HttpResponse(NoticiasServiceProvider().retorna_dados())
