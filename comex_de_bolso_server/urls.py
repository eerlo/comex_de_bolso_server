from django.conf.urls import patterns, include, url
from django.contrib import admin
from dados.views import (RetornaMoedasView, RetornaEmpresasView,
    RetornaNoticiasView)
from django.views.decorators.cache import cache_page

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'comex_de_bolso_server.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/moedas/$',
        (RetornaMoedasView.as_view())
    ),
    url(r'^api/empresas//$',
        (RetornaEmpresasView.as_view())
    ),
    url(r'^api/noticias/$',
        RetornaNoticiasView.as_view()
    )
)
